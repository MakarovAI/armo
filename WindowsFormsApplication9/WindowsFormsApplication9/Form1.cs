﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string catalog = textBox1.Text;

            string fileName = textBox2.Text;

            string textinfile = textBox3.Text;

            
            DirectoryInfo dir = new DirectoryInfo(catalog);
            List<string> fileList = new List<string>();
            string searchText = textinfile;
            foreach (var file in dir.GetFiles(fileName, SearchOption.AllDirectories))
            {
                fileList.Add(file.FullName);
            }

            foreach (var file in fileList)
            {
                string tmp = File.ReadAllText(file);
                if ((tmp.IndexOf(searchText, StringComparison.CurrentCulture) != -1))
                {
                    FileInfo F2 = new FileInfo(file);

                    listView1.Items.Add(F2.Name + " " + F2.FullName + " " + F2.Length + "_байт" +
                     " Создан: " + F2.CreationTime);
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
      
    }
    }
